# PASS-ipam-service-API
API for PASS-ipam service running in AWS

## Deployment
Deployed to AWS through gitlab-ci pipeline


![architecture](./PASS-ipam-service-architecture-PASS-ipam-service-API.drawio.png)