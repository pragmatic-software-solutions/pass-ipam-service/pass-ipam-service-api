from api_response import HttpResponse

def handler(event, context):
    print("Hello World!")
    response = HttpResponse()
    response.status_code = 200
    response.body = "Hello World"
    return response.get_dict_response()
